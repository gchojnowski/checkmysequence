#!/usr/bin/env python

from setuptools import setup, find_packages

import subprocess
import os,sys,re
sys.path.insert(0, f'{os.path.dirname(__file__)}/checkmysequence')
import checkmysequence

def get_git_describe(abbrev=7):
    import checkmysequence.version
    return checkmysequence.version.__version__

    try:
        p = subprocess.Popen(['git', 'describe', '--abbrev=%d' % abbrev],
                  stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.stderr.close()
        line = p.stdout.readlines()[0]
        version = line.strip().decode('ascii')
        # remove pep440-incompatible sha tag
        if len(version.split('-'))>1:
            version = ".".join(re.split(r'[-.]',version)[:-1])
        with open("checkmysequence/version.py", "w") as ofile:
            ofile.write("__version__=\"%s\""%version)
        return version
    except:
        return None

VERSION = get_git_describe()

setup(name='checkmysequence',
      version=VERSION,
      description='protein sequence assignment validation in EM and MX models',
      url='https://gitlab.com/gchojnowski/checkmysequence',
      author='Grzegorz Chojnowski',
      author_email='gchojnowski@embl-hamburg.de',
      license='BSD',
      packages=['checkmysequence'],
      install_requires=['findmysequence @ git+https://gitlab.com/gchojnowski/findmysequence.git', 'doublehelix @ git+https://gitlab.com/gchojnowski/doublehelix.git'],
      entry_points={
          "console_scripts": [
            "checkmysequence = checkmysequence.__main__:main",
            ],
      }
     )
