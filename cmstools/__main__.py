
__author__ = "Grzegorz Chojnowski"
__date__ = "01 Sep 2021"

import os,sys

from optparse import OptionParser, OptionGroup, SUPPRESS_HELP

import cmstools

sys.path.insert(2, '%s/../findmysequence/fmslib' % cmstools.ROOT)
sys.path.insert(2, '%s/../findmysequence' % cmstools.ROOT)
from fmslib import xyz_utils
from fmslib import sequence_utils
from fmslib import nn_predict

import h5py
import numpy as np

import xyz_utils
import sequence_utils

import iotbx
from cctbx  import maptbx
from cctbx.array_family import flex
from iotbx import ccp4_map

cmstools_header="""
# checkMySequence.tools(v%s): dev utilities for checkMySequence
"""%(cmstools.__version__)

def parse_args():
    """setup program options parsing"""
    parser = OptionParser(usage="Usage: cmstools [options]", version="cmstools %s"%cmstools.__version__)


    required_opts = OptionGroup(parser, "Required parameters (model, sequence and a map or an mtz file with labels)")
    parser.add_option_group(required_opts)

    required_opts.add_option("--modelin", action="store", \
                            dest="modelin", type="string", metavar="FILENAME", \
                  help="PDB/mmCIF model", default=None)

    required_opts.add_option("--mapin", action="store", dest="mapin", type="string", metavar="FILENAME", \
                  help="Map in CCP4 format", default=None)

    required_opts.add_option("--maskout", action="store", dest="maskout", type="string", metavar="FILENAME", \
                  help="Binary model mask in CCP4 format", default=None)

    (options, _args)  = parser.parse_args()
    return (parser, options)


# -----------------------------------------------------------------------------

def mask_map(modelin, mapin, maskout, atom_radius=3, mask_model=False, binary_model_mask=False, cctbx_sel_string=None, apply_sigma_scaling=False, verbose=True):

    xyzo = xyz_utils.xyz_utils()

    xyzo.init_maps(mapin=mapin)
    ph,_symm = xyzo.read_ph(modelin)

    _map = xyzo.map_data
    map_data = xyzo.map_data


    if cctbx_sel_string:
        sel_cache = ph.atom_selection_cache()
        isel = sel_cache.iselection
        selected_atoms = ph.select(isel(cctbx_sel_string)).atoms()
    else:
        selected_atoms = ph.atoms()


    atm_sel = maptbx.grid_indices_around_sites(
                        unit_cell=xyzo.uc,
                        fft_n_real=_map.all(),
                        fft_m_real=_map.focus(),
                        sites_cart=flex.vec3_double(selected_atoms.extract_xyz()),
                        site_radii=flex.double(selected_atoms.size(), atom_radius))


    if mask_model: # set to zero grid point around the model
        solvent_sel = flex.bool(map_data.size(), False)
        solvent_sel.set_selected(mir_sel, True)

    else: # ... or and everything in the solvent region
        solvent_sel = flex.bool(map_data.size(), True)
        solvent_sel.set_selected(atm_sel, False)

    map_data.as_1d().set_selected(solvent_sel, 0)
    if binary_model_mask: map_data.as_1d().set_selected(~solvent_sel, 1)

    if apply_sigma_scaling:
        s = maptbx.statistics(map_data)
        print(" ==> Applying sigma scaling")
        map_data = maptbx.copy(map=(map_data-s.mean())/s.sigma(),
                               result_grid=map_data.accessor()).as_double()

    iotbx.ccp4_map.write_ccp4_map(
                file_name=maskout,
                unit_cell=xyzo.uc,
                space_group=xyzo.symm.space_group_info().group(),
                gridding_first=map_data.accessor().origin(),
                gridding_last=map_data.accessor().last(),
                map_data=map_data,
                labels=flex.std_string(["%s masked using %s with %fA radius" % (mapin, modelin, atom_radius)]))

    ## clean tmp files
    #os.remove( os.path.join(self.workdir,_map4mask) )

# -----------------------------------------------------------------------------

def main():
    print(cmstools_header)

    (parser, options) = parse_args()

    print( " ==> Command line: cmstools %s" % (" ".join(sys.argv[1:])) )

    mask_map(mapin=options.mapin, modelin=options.modelin, binary_model_mask=True, maskout=options.maskout)


if __name__=="__main__":
    main()
