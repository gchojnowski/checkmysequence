# Sequence assignment validation in cryo-EM models with checkMySequence

- [How to cite](#how-to-cite)
- [Installation](#installation)
   - [installing from source](#source-installation)
   - [troubleshooting](#troubleshooting)
- [Test your installation](#test-your-installation)
- [How to use checkMySequence](#how-to-use-checkmysequence)


# How to cite

Sequence assignment validation in cryo-EM models with checkMySequence\
Grzegorz Chojnowski
[Acta Cryst. (2022). D78](https://scripts.iucr.org/cgi-bin/paper?S2059798322005009)


Sequence assignment validation in protein crystal structure models with checkMySequence\
Grzegorz Chojnowski
[biorxiv (2023)](https://www.biorxiv.org/content/10.1101/2023.02.17.528951v1)

# Installation

The program was tested mostly on Linux and MacOS. It will also work on MS Windows inlcuding the Windows Subsystem for Linux (WLS).

The program has (soft) dependencies on various tools distributed with [CCP4 suite](https://www.ccp4.ac.uk/) e.g. the HAMMER suite. If you don't have CCP4 installed, don't worry; the program will run normally.

For handling software dependencies *checkMySequence* requires **conda** or **miniconda** environments. Detailed installation instructions for each of them can be found [here](https://docs.conda.io/en/latest/index.html) and [here](https://docs.conda.io/en/latest/miniconda.html).

**M1/M2 users:** please install intel version of conda/miniconda (it's fully compatible with your system). Not all checkMySequence dependencies are currently available for arm architecture.

After installing conda (if needed) create and activate an enironment:

```
conda create -n checkmysequence
conda activate checkmysequence
```

alternatively, you can use miniconda. It doesn't require any changes to you system and it's fully contained in a local directory. To install it download an installer from [here](https://docs.conda.io/en/latest/miniconda.html) and run the following (linux example):

```
bash Miniconda3-latest-Linux-x86_64.sh -b -p miniconda_checkmysequence
. miniconda_checkmysequence/bin/activate
```

Now, you can proceed with the installation instructions below

### Source installation 

##### Install dependencies

CheckMySequence dependes on [findMySequence](https://gitlab.com/gchojnowski/findmysequence) and [doubleHelix](https://gitlab.com/gchojnowski/doublehelix) projects. They will be installed automatically if not yet available in the current conda environment. First, install dependencis using conda

```
conda install -c conda-forge -c bioconda hmmer cctbx-base scipy scons infernal h5py boost
```

Next, clone repository and run installation script. It will download findMySequence and compile additional python modules.

```
git clone https://gitlab.com/gchojnowski/checkmysequence.git
cd checkmysequence
python -m pip install .
```
##### Troubleshooting

If the standard installation procedure above fails (can happen on older Linux installations or WLS), try to install latest c/c++ compilers with conda and rerurn instalation script

```
conda install -c conda-forge gcc gxx
python -m pip install .
```

### Test your installation

```
checkmysequence --test
```

# How to use checkmysequence

Given a protein model, cryo-EM reconstruction and all reference protein sequences the program checks plausibility of the model sequence

```
checkmysequence --mapin emd_1234.map --modelin 1abc.cif --seqin sequences.fa
```

for an MX structures checkMySequende requires map coefficients in mtz format, e.g.

```
checkmysequence --mtzin 1abc.mtz --labin FWT,PHWT --modelin 1abc.cif --seqin sequences.fa
```

you can also test a PDB deposited structure (map, model, and target sequences will be downloaded automatically)

```
checkmysequence --pdbid 1cex
```


The programs tests the following aspects of your model sequende assignment:

1. Unidentified protein chains (e.g. missing in a reference set or medelled outside density)
1. Sequence mismatches (differences between model and target sequence)
1. Residue indexing issues (e.g. 0-length gaps)
1. Sequence register errors

and prints results in a simple text format, for example:

```
********************************************************************************
*********************************   SUMMARY   **********************************
********************************************************************************

 ==> Unidentified chains; check input sequences and model-to-map fit

	e/2:51
	g/3:39

 ==> Chains with sequence mismatches; you will have to fix them first!
  model       KDNVVQMMNEKKSFDVSDFPKVYLTTAVEEDLDT--
              ||||||||||||||||||||||||||*|||||||
  refseq      KDNVVQMMNEKKSFDVSDFPKVYLTTTVEEDLDTRG

 ==> Possible sequence assignment issues
  - Fragment N/5-24 has a chain break at N/11 and no residue indexing gap, check!

  model       ---------VELTEEE-LYISKKNLLFKRF----------     
                       ||||||| |||||||||||||               
  refseq      AAAAAAMPRVELTEEEKLYISKKNLLFKRFVEPGRLCLIE        


 - Fragment F/356-395 is shifted by -4 residues [-log(p-value)=1.99]
   model seq 356-395
       sknkkEKRVQKQIQKKELQKINHDYYKGVAKAVKKKKKREEKKAKskktanqavi
     new seq 360-399
       sknkkekrvQKQIQKKELQKINHDYYKGVAKAVKKKKKREEKKAKSKKTanqavi
```

You can add ```--plot``` keyword to create a handy bar-plot in pdf format.

![image-1.png](./media/image.png)

The bars are proportional to the -log(p-value) and the higher they are, the more reliable the sequence of a fragment. If a bar is red, a high-confidence sequence assigned to a fragment and corresponding model sequence differ. This may be an error!

(C) 2022 Grzegorz Chojnowski  
