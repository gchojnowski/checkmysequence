
__author__ = "Grzegorz Chojnowski"
__date__ = "27 Apr 2021"

import os,sys,re
import json
import time
import gzip
from datetime import timedelta
from shutil import which

from io import StringIO

try:
    from termcolor import colored
    COLOR=1
except:
    COLOR=0

from optparse import OptionParser, OptionGroup, SUPPRESS_HELP

import checkmysequence
try:
    import checkmysequence.version
    cms_version = checkmysequence.version.__version__
except:
    cms_version="dev"

try:
    import findmysequence.version
    fms_version = findmysequence.version.__version__
except:
    fms_version="unknown"

try:
    import doubleHelix.version
    import doubleHelixLib.sequence_utils
    import doubleHelixLib.cmatch_rna
    doublehelix_version = doubleHelix.version.__version__
    infernal_available_info = f"""INFERNAL is {'' if doubleHelixLib.sequence_utils.INFERNAL_AVAILABLE else "NOT "}available{" (from CCP4)" if doubleHelixLib.sequence_utils.INFERNAL_FROM_CCP4 else ""}"""
except:
    doublehelix_version = None
    infernal_available_info = "WARNING: Nucleic acid sequence validation is NOT available"


import fmslib.sequence_utils

import numpy as np

import iotbx
from iotbx.bioinformatics import any_sequence_format
from cctbx  import maptbx
from cctbx.array_family import flex
from iotbx import ccp4_map
from mmtbx.alignment import align

import tempfile
import subprocess

POLYALA_LEN=10

CCP4_ENV = os.environ.get('CCP4', None)

cms_header=f"""
# checkMySequence ({cms_version}): a tool for sequence assignment validation in EM and MX models of macromolecules

        findMySequence {fms_version}
        doubleHelix    {doublehelix_version}

HMMER is {'' if fmslib.sequence_utils.HMMER_AVAILABLE else "NOT "}available{" (from CCP4)" if fmslib.sequence_utils.HMMER_FROM_CCP4 else ""}
{infernal_available_info}
"""

def parse_args():
    """setup program options parsing"""
    parser = OptionParser(usage="Usage: checkMySequence [options]", version="checkMySequence %s"%cms_version)


    required_opts = OptionGroup(parser, "Required parameters (model, sequences and a map)")
    parser.add_option_group(required_opts)

    required_opts.add_option("--modelin", action="store", \
                            dest="modelin", type="string", metavar="FILENAME", \
                  help="PDB/mmCIF model", default=None)

    required_opts.add_option("--seqin", action="store", dest="seqin", type="string", metavar="FILENAME", \
                  help="target sequences in FASTA format", default=None)

    required_opts.add_option("--mapin", action="store", dest="mapin", type="string", metavar="FILENAME", \
                  help="map in CCP4/MRC format", default=None)

    required_opts.add_option("--mtzin", action="store", dest="mtzin", type="string", metavar="FILENAME", \
                  help=[SUPPRESS_HELP,"input SFs in MTZ format"][1], default=None)

    required_opts.add_option("--labin", action="store", dest="labin", type="string", metavar="FWT,PHWT", \
                  help=[SUPPRESS_HELP,"MTZ file column names"][1], default=None)



    extra_opts = OptionGroup(parser, "Extra options")
    parser.add_option_group(extra_opts)

    extra_opts.add_option("--fraglen", dest="fraglen", type="int", default=None, metavar="VALUE", \
                  help="starting length of test-fragments [default: 10/MX 20/EM]")

    extra_opts.add_option("--max_fraglen", dest="max_fraglen", type="int", default=60, metavar="VALUE", \
                  help="maximum length of test-fragments [default: %default]")

    extra_opts.add_option("--plot", action="store_true", dest="plot", default=False, \
                  help="generate graphical report")

    extra_opts.add_option("--select", action="store", dest="selstr", type="string", metavar="STRING", \
                  help="model fragment selection string"\
                       " eg. \"chain A and resi 10:50\" [default: %default]", default="all")

    # utility opt: check a PDB deposit by PDB_ID
    extra_opts.add_option("--pdbid", action="store", dest="pdbid", type="string", metavar="PDB_ID", \
                  help="Download and check a model directly from PDB"\
                        " (use --keep to store the data in curent directory)", default=None)

    extra_opts.add_option("--jsonout", action="store", dest="jsonout_fname", type="string", metavar="FILENAME", \
                  help="""output filename with the validation results in json format (machine readable)""", default=None)

    # developer opts: lots of sto info
    parser.add_option("--debug", action="store_true", dest="debug", default=False, \
                  help=SUPPRESS_HELP)

    parser.add_option("--keep", action="store_true", dest="keep", default=False, \
                  help=SUPPRESS_HELP)

    parser.add_option("--datadir", action="store", dest="datadir", type="string", metavar="DIRNAME", default='.', \
                  help=SUPPRESS_HELP if 1 else "Check availability of any data required by --pdbid before downloading it here")

    # developer opts: print residue indices relative to target sequence instead of PDB/mmCIF resids
    parser.add_option("--refseq_resi", action="store_true", dest="refseq_resi", default=False, \
                  help=SUPPRESS_HELP)

    # developer opt: use local resolution map to print mean locres of fragments
    required_opts.add_option("--resmapin", action="store", dest="resmapin", type="string", metavar="FILENAME", \
                  help=SUPPRESS_HELP, default=None)

    # developer opt: RMSD value of a random coordinate bias
    required_opts.add_option("--shake", action="store", dest="shake_rms", type="float", metavar="FLOAT", \
                  help=SUPPRESS_HELP, default=None)

    # developer opt: B-factor for map sharpening
    required_opts.add_option("--badd", action="store", dest="badd", type="float", metavar="FLOAT", \
                  help=SUPPRESS_HELP, default=0)

    parser.add_option("--test", action="store_true", dest="test", default=False, \
                  help="a simple test")



    (options, _args)  = parser.parse_args()
    return (parser, options)


class sharpen_with_coot:
    '''
        uses old-good-citable COOT sharpen/blurr - for testing only!

    '''

    def __init__(self, mapin, badd):
        scm_template = """(set-map-sharpening-scale-limit 300)\n(define mapFile \"%(mapin_abs)s\" )\n(define imol (handle-read-ccp4-map mapFile 0))\n(define imolmod (sharpen-blur-map imol %(badd)f))\n(export-map imolmod \"%(mapout_abs)s\")\n(coot-real-exit 0)"""
        coot_sh = "coot --no-state-script --script %(tmpdirname)s/coot.scm --no-graphics --no-guano --no-splash-screen"

        with tempfile.TemporaryDirectory() as tmpdirname:
            print(" ==> Using COOT to sharpen input map by %f"%badd)
            print(" ==> Created temp directory", tmpdirname)
            mapout_abs=os.path.join(tmpdirname, "processed_map.map")
            mapin_abs=os.path.abspath(mapin)
            with open( os.path.join(tmpdirname, 'coot.scm'), 'w' ) as scm_fname:
                scm_fname.write(scm_template%locals())
            ret = subprocess.run(coot_sh%locals(), shell=True, capture_output=True, text=True, check=True)
            self.tco = sequence_checker(mapin=mapout_abs, mtzin=None, labin=None)

# -----------------------------------------------------------------------------


class xyz_shaker:
    '''
        simple helper function for testing, adds to a model atomic coordinates
        a random bias with user-given rmsd
    '''

    def __init__(self, ph, shake_rms):

        _ph = ph.deep_copy()
        self.xrs = ph.extract_xray_structure()
        self.ph_mod = _ph
        self.xrs.shake_sites_in_place(rms_difference=shake_rms)

        # resulting (deeply shaken) ph
        self.ph_mod.atoms().set_xyz(self.xrs.sites_cart())

# -----------------------------------------------------------------------------


class resmap:

    def __init__(self, resmapin):
        inp_ccp4_map = ccp4_map.map_reader(file_name=resmapin)
        self._map_data = inp_ccp4_map.data.as_double()
        self._symm = inp_ccp4_map.crystal_symmetry()

    def median_locres(self, residues, atom_radius=2):

        xyz = flex.vec3_double([])
        for _r in residues:
            xyz.extend(_r.atoms().extract_xyz())

        mir_sel = maptbx.grid_indices_around_sites(
                                        unit_cell   =   self._symm.unit_cell(),
                                        fft_n_real  =   self._map_data.all(),
                                        fft_m_real  =   self._map_data.focus(),
                                        sites_cart  =   flex.vec3_double(xyz),
                                        site_radii  =   flex.double(len(xyz), atom_radius))

        _m = np.array(self._map_data.select(mir_sel))
        m = _m[_m<100]
        outliers = _m[_m>99]

        if not m.size: return (0, 0, 0, 0)

        return (np.median(m), m.mean(), m.size, outliers.size)


# -----------------------------------------------------------------------------


class sequence_checker:

    def __init__(self, mapin=None, mtzin=None, labin=None, badd=0):

        (parser, options) = parse_args()

        self.mapin = mapin
        self.mtzin = mtzin

        self.m2so = fmslib.sequence_utils.model2sequence( mapin=mapin, mtzin=mtzin, labin=labin, badd=badd )
        if doublehelix_version:
            #self.m2so_na = doubleHelixLib.sequence_utils.model2sequence( mapin=mapin, mtzin=mtzin, labin=labin, badd=badd, xyzutils_obj=self.m2so._xyz_utils)
            self.m2so_na = doubleHelixLib.sequence_utils.model2sequence(xyzutils_obj=self.m2so._xyz_utils)
        else:
            self.m2so_na = None

        self.aa_names_array =  np.array(self.m2so._xyz_utils.standard_aa_names)
        self.every_aa_pbty_is_one = dict( [(_a, 1.0) for _a in self.aa_names_array] )
        self.every_na_pbty_is_one = {'R': 1.0, 'Y': 1.0}

    # -------------------------------------

    def prepare_model(self, modelin, selstr="all"):

        ph, symm = self.m2so._xyz_utils.read_ph( modelin )

        sel_cache = ph.atom_selection_cache()
        isel = sel_cache.iselection
        self.ph_selected = ph.select(isel(selstr))

        self.chain_frags = self.m2so._xyz_utils.find_chain_fragments(self.ph_selected, verbose=False)

    # -------------------------------------

    def find_target_sequence(self, input_ph, seqin_fname, selstr='all', hits=1, debug=False):

        # -----------------
        #   Nulceic acids
        # -----------------

        if self.m2so_na:
            na_chain_frags = self.m2so_na._xyz_utils.find_chain_fragments_na(input_ph, verbose=False)
            if na_chain_frags:

                # exclude NA-like ligands (lone bases)
                if not sum(map(len, na_chain_frags))>1: return None, [], False

                # infernal doesn't work for short sequences; try naive alignment instead
                if input_ph.overall_counts().get_n_residues_of_classes('common_rna_dna')>100:
                    # pre-filter protein sequences. this is not optimal, but needs to be done as infernal can pick aa targets
                    with tempfile.NamedTemporaryFile(prefix='doubleHelix_seqin_', suffix='.fasta', mode='w', delete=False) as ofile:
                        _obj, _err = any_sequence_format(file_name=seqin_fname)
                        for _s in _obj:
                            if len(set(_s.sequence))>6: continue
                            ofile.write('>%s\n%s\n'%(_s.name, _s.sequence))

                    results = self.m2so_na.query_infernal(input_ph, hmm=True, seqdb_fn=ofile.name, selstr='all', depth=0, tophits_sto=1)
                    os.unlink(ofile.name)

                    return results, na_chain_frags, False

                else:
                    self.m2so_na.model2msa( input_ph, selstr='all', verbose=debug )
                    _obj, _err = any_sequence_format(file_name=seqin_fname)
                    results=[]
                    for _seq in _obj:
                        if len(set(_seq.sequence))>6: continue
                        # ugly, but works... better sol requires an update on doubleHelix side
                        with tempfile.NamedTemporaryFile(prefix='doubleHelix_seqin_', suffix='.fasta', mode='w', delete=False) as ofile:
                            ofile.write('>%s\n%s\n'%(_seq.name, _seq.sequence))
                        _res = self.m2so_na.align_frags(seqin_fname=ofile.name,depth=1,dna=False,dryrun=True)
                        os.unlink(ofile.name)
                        for _r in _res:
                            results.append( (_r['reference_sequence_name'], _r['pvalue'], f">{_r['reference_sequence_name']}\n{_r['reference_sequence']}") )

                    return results, na_chain_frags, False

        # -----------------
        #     Proteins
        # -----------------

        # return None if not AA or NA (tested above)
        aa_chain_frags = self.m2so._xyz_utils.find_chain_fragments(input_ph, verbose=False)
        if not aa_chain_frags: return None, [], False

        msa_string = self.m2so.model2msa( input_ph, selstr=selstr, verbose=False )
        res = self.m2so.query_msa( msa_string, db_fname=seqin_fname, tophits_sto=hits, verbose=debug)
        return res, aa_chain_frags, True

    # -------------------------------------

    def parse_seqstr(self, target_sequence):

        sequence_processed = []
        for line in target_sequence.split('\n'):
            if line.startswith('>'): continue
            sequence_processed.append(line.replace(' ', ''))

        return "".join(sequence_processed)

    # -------------------------------------

    def prepare_target_sequence(self, seqin_fname=None, seqin_string=None, is_protein=True):

        ry_array =  np.array(['R', 'Y'])

        if seqin_fname:
            with open(seqin_fname, 'r') as ifile:
                target_sequence=ifile.read()
        else:
            target_sequence=seqin_string


        # ads 'dummy buffer' to the target seq - important when target sequence and fragment length are close
        if is_protein:
            self.target_sequence_processed = 'A'*POLYALA_LEN + self.parse_seqstr(target_sequence) + 'A'*POLYALA_LEN
        else:
            self.target_sequence_processed = self.parse_seqstr(target_sequence)

        #self.target_sequence_processed = self.parse_seqstr(target_sequence)

        rng = np.random.default_rng(seed=321)

        if is_protein:
            self.random_sequence_array = np.array([self.aa_names_array==self.m2so.tgo[_a] for _a in fmslib.sequence_utils.random_sequence], dtype=float)
            self.random_sequence_array = rng.choice(self.random_sequence_array, 5000, replace=True, axis=0)

            self.target_sequence_array = np.array([self.aa_names_array==self.m2so.tgo[_a] for _a in self.target_sequence_processed], dtype=float)
        else:
            self.random_sequence_array = np.array([ry_array==_a for _a in doubleHelixLib.sequence_utils.random_ry_sequence], dtype=float)
            self.random_sequence_array = rng.choice(self.random_sequence_array, 10000, replace=True, axis=0)

            self.target_sequence_array = np.array([ry_array==self.m2so_na._xyz_utils.na2ry.get(_a, rng.choice(['R', 'Y'])) for _a in self.target_sequence_processed], dtype=float)

        # target_sequence_array: if there are UNKS in taregt sequence (e.g. 'X') the code above will select them for random sequence
        # and almost all base scores will be set to 1e-6


    # -------------------------------------

    def frag2seq(self, frag, tophits=None):

        frag_scores = []
        for res in frag:
            rid = "%s_%i" % (res.parent().id, res.resseq_as_int())
            _score = self.m2so.residue_scores_dicts.get(rid, None)
            if _score is None:
                print("     WARNING: Ignoring incomplete or unknown residue: %s/%i" % (res.parent().id, res.resseq_as_int()))
                # there is smthg wrong with the resi model and pbties cannot be estimated (e.g. missign backbone atoms)
                # placing dummy pbty=1 for all resi-types to keep correct register
                frag_scores.append( self.every_aa_pbty_is_one )
                continue
            frag_scores.append( _score  )

        frag_scores_array = np.array([[np.log(np.clip(_s[_a],1e-33,1.0)) for _a in self.aa_names_array] for _s in frag_scores], dtype=float)
        alignment_scores = np.array(self.m2so.calc_alignment_scores(frag_scores_array, self.target_sequence_array))

        base_scores = self.m2so.calc_alignment_scores(frag_scores_array, self.random_sequence_array)
        _random_scores_mean,_random_scores_std = np.mean(base_scores), np.std(base_scores)

        # gets rid of frags aligned entirely to UNKS in target sequence;
        # these by defailt have a tiny positive score 1e-6, which (unlike -9999) is larger than any log(pbty)
        alignment_scores[alignment_scores>0] = -9999

        if tophits is None:
            best_match_index = np.argmax(alignment_scores)
            pvalue, z, n = self.m2so.calc_Gumbel_pvalue(alignment_scores, _random_scores_mean, _random_scores_std)
            return -np.log(pvalue)/np.log(10), best_match_index

        else:
            _sorted_args = np.argsort(alignment_scores)[::-1]
            _pv_array = np.array([ self.m2so.calc_Gumbel_pvalue(alignment_scores[_sorted_args[_i:]], _random_scores_mean, _random_scores_std)[0] for _i in range(tophits) ])
            return -np.log(_pv_array)/np.log(10), _sorted_args[:tophits]

    # -------------------------------------

    def frag2seq_na(self, frag, tophits=None):

        frag_scores = []
        for res in frag:
            rid = "%s_%i" % (res.parent().id, res.resseq_as_int())
            _score = self.m2so_na.residue_scores_dicts.get(rid, None)
            if _score is None:
                print("     WARNING: Ignoring incomplete or unknown residue: %s/%i" % (res.parent().id, res.resseq_as_int()))
                # there is smthg wrong with the resi model and pbties cannot be estimated (e.g. missign backbone atoms)
                # placing dummy pbty=1 for all resi-types to keep correct register
                frag_scores.append( {'R': 1.0, 'Y': 1.0} )
                continue
            frag_scores.append( _score  )

        atoms_pdbstr=[]
        for _r in frag:
            for _a in _r.atoms():
                atoms_pdbstr.append(_a.format_atom_record())

        cobject = doubleHelixLib.cmatch_rna.match(target_pdbstr = "\n".join(atoms_pdbstr))
        # do not try to use nproc>1, thresding is weakref-broken
        stems_bps_and_stacks= self.m2so_na.find_wc_basepairs_for_assignment_and_identification(cobject, dna=False, depth=0, nproc=1, verbose=False, silent=True)

        basepairs = []
        for _ridx, _r in enumerate(cobject.target_resids):
            _rid = "/".join(_r)
            _paired_rid = stems_bps_and_stacks['bp_dict'].get(_rid, None)
            if _paired_rid:
                _paired_ridx = cobject.target_resids.index(tuple(_paired_rid.split('/')[:-1]))
                basepairs.append( sorted([_ridx, _paired_ridx]) )

        frag_scores_array = np.array([[np.log(np.clip(_s[_a],1e-33,1.0)) for _a in ['R', 'Y']] for _s in frag_scores], dtype=float)
        alignment_scores = np.array(self.m2so_na.calc_alignment_scores(frag_scores_array, self.target_sequence_array, basepairs=basepairs))

        base_scores = self.m2so_na.calc_alignment_scores(frag_scores_array, self.random_sequence_array, basepairs=basepairs)
        _random_scores_mean,_random_scores_std = np.mean(base_scores), np.std(base_scores)

        # gets rid of frags aligned entirely to UNKS in target sequence;
        # these by defailt have a tiny positive score 1e-6, which (unlike -9999) is larger than any log(pbty)
        alignment_scores[alignment_scores>0] = -9999

        if tophits is None:
            best_match_index = np.argmax(alignment_scores)
            pvalue, z, n = self.m2so_na.calc_Gumbel_pvalue(alignment_scores, _random_scores_mean, _random_scores_std)
            return -np.log(pvalue)/np.log(10), best_match_index

        else:
            tophits=min(tophits, len(alignment_scores))
            _sorted_args = np.argsort(alignment_scores)[::-1]

            # if there are only zeros in the scores (no information at all - can happen)
            if not np.any(frag_scores_array):
                _pv_array=np.ones(tophits)
            else:
                _pv_array = np.array([ self.m2so_na.calc_Gumbel_pvalue(alignment_scores[_sorted_args[_i:]], _random_scores_mean, _random_scores_std)[0] for _i in range(tophits) ])

            return -np.log(_pv_array)/np.log(10), _sorted_args[:tophits]

    # -----------------------------------------------------------------------------

    def absidx(self, fragseq, refseq, verbose=False):
        # WARNING 1-based indexing!

        #find fragment index in reference sequence; align model frag sequence to reference
        align_obj = align(seq_b = refseq,
                          seq_a = fragseq, similarity_function="identity")

        alignment = align_obj.extract_alignment()
        si = 100*alignment.calculate_sequence_identity(skip_chars=['X'])

        mysto = StringIO()
        alignment.pretty_print(block_size=100, top_name="  model", bottom_name="  refseq", out=mysto, show_ruler=False)
        mysto.seek(0)
        align_sto = mysto.read()

        # lookahead takes care of single-chair breaks (d|i)
        match_template=r"(?=(?:[^m]|^)(?P<matches>m+?)(?:[^m]|$))"
        m = re.findall(match_template, alignment.match_codes)
        if m and len(m)>1:
            break_idx=len(m[0])-1
        else:
            break_idx=-3


        #shift_template=r"^(?P<lead_dash>\-+?)[^\-]+"
        #shift_template=r"^(?P<lead_dash>\-+?)[^\-].*[^\-](?P<tail_dash>\-+?)$"
        # this will match when frag hits start/end of ref seq - important for NAs wout poly-a
        # ---------PKIVKWDRDMGGGG----------------EHVSFH----------
        # 1111111112222222222222222222222222222222222223333333333
        # 1 - lead_dash
        # 3 - tail_dash
        shift_template=r"^(?P<lead_dash>\-*?)([^\-].*[^\-])(?P<tail_dash>\-*?)$"
        match = re.search(shift_template, alignment.a)
        if match:
            return len(match.group('lead_dash'))+1, len(alignment.a)-len(match.group('tail_dash')), si, align_sto, break_idx

        return 1, len(alignment.a), si, align_sto, break_idx


    def uprange(self, istr, start, end, base_idx, color=None):
        if color:
            return istr[:start-base_idx-1] + colored(istr[start-base_idx-1:end-base_idx-1], color) + istr[end-base_idx-1:]
        else:
            return istr[:start-base_idx-1].lower() + istr[start-base_idx-1:end-base_idx-1].upper() + istr[end-base_idx-1:].lower()

    # -----------------------------------------------------------------------------

    def results2report(self, frag_results,
                             chain_results,
                             modelin            =   None,
                             rowlen             =   40,
                             logpv_threshold    =   0.55,
                             logpv_threshold_na =   0.25,
                             colors             =   COLOR,
                             max_shift          =   13,
                             refseq_resi        =   False,
                             debug              =   False):
        '''
            function creating a dictionary with sequence assignment analysis report (with all heuristics)

            {
                "clean_report": false,
                "indexing_issues": {
                    "protein": [
                        {
                            "chain_id_reference": "B",
                            "resid_start_reference": 133,
                            "resid_end_reference": 152,
                            "break_idx": 148,
                            "align_sto": ""
                        }
                    ]
                },
                "unidentified_chains": {
                    "clean_report": false,
                    "na": [
                        {
                            "chain_id_reference": "P",
                            "resid_start_reference": "10",
                            "resid_end_reference": "20"
                        },
                        {
                            "chain_id_reference": "T",
                            "resid_start_reference": "8",
                            "resid_end_reference": "21"
                        }
                    ]
                },
                "register_shifts": {
                    "protein": [
                        {
                            "chain_id_reference": "A",
                            "resid_start_reference": 906,
                            "resid_end_reference": 919,
                            "resid_start_new": 915,
                            "resid_end_new": 928,
                            "shift": 9,
                            "mlogpv": 1.0068789995424352,
                            "tracing_issues": false,
                            "si": 100.0,
                            "model_seq": "dmysvMLTNDNTSRYWEPEfyeamytphtvlqgg",
                            "new_seq": "dmysvmltndntsrYWEPEFYEAMYTPHtvlqgg"
                        }
                    ]
                },
                "sequence_mismatches": {
                    "protein": [
                        {
                            "chain_id_reference": "C",
                            "resid_start_reference": "2",
                            "resid_end_reference": "64",
                            "seq2ref_si": 96.7741935483871,
                            "evalue": 8.8e-25,
                            "alignment": ""
                        }
                    ]
                },
                "raw_results": {
                    "protein": {
                        "A": [
                            {
                                "s": 800,
                                "e": 819,
                                "mlogpv": 3.719042325647506,
                                "match": true,
                                "error": false
                            },
                            {
                                "s": 805,
                                "e": 824,
                                "mlogpv": 2.987087628879408,
                                "match": true,
                                "error": false
                            }
                        ],
                        "B": [
                            {
                                "s": 78,
                                "e": 97,
                                "mlogpv": 2.3199604132705773,
                                "match": true,
                                "error": false
                            },
                            {
                                "s": 83,
                                "e": 102,
                                "mlogpv": 2.4086148958495652,
                                "match": true,
                                "error": false
                            }
                         ]
                    }
                }
            }
        '''

        results_dict = {'clean_report':True,
                        'indexing_issues':{},
                        'unidentified_chains':{},
                        'register_shifts':{},
                        'sequence_mismatches':{},
                        'tracing_issues':{},
                        'raw_results':{}}

        #
        # 1 UNIDENTIFIED CHAINS; CHECK INPUT SEQUENCES AND MODEL-TO-MAP FIT
        #
        for ch in chain_results:
            if ch['refseq_id'] is None:

                # ==> update results dict: UNIDENTIFIED_CHAINS
                results_dict['clean_report'] = False
                results_dict['unidentified_chains']['clean_report'] = False
                _a = results_dict['unidentified_chains'].setdefault('protein' if ch['is_protein'] else 'na', [])
                _a.append( dict([(k,ch[k]) for k in ['chain_id_reference','resid_start_reference','resid_end_reference']]) )



        #
        # 2 SEQUENCE_MISMATCHES
        #
        for ch in chain_results:
            if ch['refseq_id'] is None: continue
            if ch['seq2ref_si'] is None or ch['seq2ref_si']<100:

                align_obj = align(seq_b=ch['reference_sequence'], seq_a=ch['chain_sequence'], similarity_function="identity")
                alignment = align_obj.extract_alignment()

                mysto = StringIO()
                alignment.pretty_print(block_size=80, top_name="  model", bottom_name="  refseq", out=mysto)
                mysto.seek(0)
                _alistr = mysto.read()

                # ==> update results dict: SEQUENCE_MISMATCHES
                results_dict['clean_report'] = False
                _a = results_dict['sequence_mismatches'].setdefault('protein' if ch['is_protein'] else 'na', [])
                _d = dict([(k,ch[k]) for k in ['chain_id_reference','resid_start_reference','resid_end_reference','seq2ref_si','evalue']])
                _d['alignment'] = _alistr
                _a.append( _d )


        #
        # 3 POSSIBLE SEQUENCE ASSIGNMENT ISSUES AND RAW RESULTS
        #
        frag_results_filtered = []
        for res in frag_results:

            if res['is_protein']:
                selected_logpv_threshold = logpv_threshold
            else:
                selected_logpv_threshold = logpv_threshold_na

            # ==> update results dict: RAW_RESULTS
            _a = results_dict['raw_results'].setdefault('protein' if res['is_protein'] else 'na', {})
            _chain = _a.setdefault(res['chain_id_reference'], [])
            _d={}
            _d['s'] = int(res['resid_start_reference'])
            _d['e'] = int(res['resid_end_reference'])
            _d['mlogpv'] = res['mlogpv']
            _d['match']=res['match']
            _d['error']=bool(not res['match'] and res['mlogpv']>selected_logpv_threshold)
            _chain.append(_d)

            if res['match']: continue
            if not res['match'] and res['mlogpv']<selected_logpv_threshold: continue

            idx_model_start, idx_model_end, si, align_sto, break_idx = self.absidx(fragseq=res['frag_sequence'], refseq=res['reference_sequence'])

            # if chain frag is broken (non-continous sequence alignment); report and continue
            if not break_idx<0:

                # select a relevant fragment of reference sequence only; useful for very long reference sequences
                idx_model_start, idx_model_end, si, align_sto, break_idx = \
                            self.absidx(fragseq=res['frag_sequence'], refseq=res['reference_sequence'][(idx_model_start-10):(idx_model_end+10)])



                # ==> update results dict: INDEXING_ISSUES
                results_dict['clean_report'] = False
                _a = results_dict['indexing_issues'].setdefault('protein' if res['is_protein'] else 'na', [])

                # check for overlapping frags (same shift will be reported in several frags)
                unique = True
                for other_res in _a:
                    if (res['resid_start_reference']<other_res['resid_start_reference']<res['resid_end_reference'] or \
                        res['resid_start_reference']<other_res['resid_end_reference']<res['resid_end_reference']) \
                        and res['chain_id_reference'] == other_res['chain_id_reference']:
                            unique=False
                            break

                if not unique: continue

                _d ={'chain_id_reference':res['chain_id_reference']}
                _d['resid_start_reference'] = int(res['resid_start_reference'])
                _d['resid_end_reference'] = int(res['resid_end_reference'])
                _d['break_idx'] = int(res['resid_start_reference']+break_idx)
                _d['align_sto'] = align_sto
                _a.append( _d )

                continue


            shift = res['idx_assignment']-idx_model_start

            # large shifts and no clear top-score p-value contrast are often due to tracing issues
            if (res['dubious'] or abs(shift)>max_shift) and res['mlogpv']<2:
                continue
                print(" - Fragment %s/%i-%i may be mistraced, check!" % (res['chain_id_reference'],
                                                                         res['resid_start_reference'],
                                                                         res['resid_end_reference']))
                print()
                continue

            # helper vars with resi indices (PDB/mmCIF or reference sequece/refseq if needed)
            if res['is_protein']:
                resid_start_reference = (res['idx_assignment']-POLYALA_LEN) if refseq_resi else res['resid_start_reference']
                resid_end_reference = (res['idx_assignment']+res['length']-POLYALA_LEN) if refseq_resi else res['resid_end_reference']
            else:
                resid_start_reference = res['idx_assignment'] if refseq_resi else res['resid_start_reference']
                resid_end_reference = res['idx_assignment']+res['length'] if refseq_resi else res['resid_end_reference']

            res['shift']=shift
            res['idx_model_start']=idx_model_start
            res['resid_start_reference']=resid_start_reference
            res['resid_end_reference']=resid_end_reference

            frag_results_filtered.append(res)


        #
        # 4/5 REGISTER SHIFTS AND TRACING ISSUES
        #
        for res in frag_results_filtered:

            # check if there are overlapping fragments with inconsistent shifts - most pbly they have tracing issues
            tracing_issues = False
            for other_res in frag_results_filtered:
                if not res['chain_id_reference'] == other_res['chain_id_reference']: continue

                if (res['resid_start_reference']<other_res['resid_start_reference']<res['resid_end_reference'] or \
                    res['resid_start_reference']<other_res['resid_end_reference']<res['resid_end_reference']) \
                    and not res['shift'] == other_res['shift']:
                        tracing_issues=True
                        break

            sequence_range = ( max(0,min(res['idx_model_start'], res['idx_assignment'])-6), max(res['idx_model_start'], res['idx_assignment'])+res['length']+5)
            seq_range = res['reference_sequence'][sequence_range[0]:sequence_range[1]]

            # ==> update results dict: REGISTER_SHIFTS AND TRACING ISSUES
            if tracing_issues:
                _a = results_dict['tracing_issues'].setdefault('protein' if res['is_protein'] else 'na', [])
            else:
                _a = results_dict['register_shifts'].setdefault('protein' if res['is_protein'] else 'na', [])

            results_dict['clean_report'] = False

            _d ={'chain_id_reference':res['chain_id_reference']}
            _d['resid_start_reference'] = int(res['resid_start_reference'])
            _d['resid_end_reference'] = int(res['resid_end_reference'])
            _d['resid_start_new'] = int(res['resid_start_reference']+res['shift'])
            _d['resid_end_new'] = int(res['resid_end_reference']+res['shift'])
            _d['shift'] = int(res['shift'])
            _d['mlogpv'] = res['mlogpv']
            _d['tracing_issues']=tracing_issues
            _d['si']=si
            _d['model_seq'] = self.uprange(seq_range, res['idx_model_start'], res['idx_model_start']+res['length'], base_idx=sequence_range[0], color=None)
            _d['new_seq']   = self.uprange(seq_range, res['idx_assignment'], res['idx_assignment']+res['length'], base_idx=sequence_range[0], color=None)
            _a.append( _d )


        return results_dict

    # -----------------------------------------------------------------------------

    def report2sto(self, results_dict):

        print()
        print('*'*80)
        print('*'*33, '  SUMMARY  ', '*'*34)
        print('*'*80)
        print()

        # 1 UNIDENTIFIED_CHAINS
        if results_dict['unidentified_chains']:
            print()
            print(" ==> Unidentified chains; check input sequences and model-to-map fit\n")
            for chtype in ['protein', 'na']:
                for error in results_dict['unidentified_chains'].get(chtype, []):
                    print( "\t%s/%s:%s"%(error['chain_id_reference'], error['resid_start_reference'], error['resid_end_reference']))

        # 2 SEQUENCE_MISMATCHES
        if results_dict['sequence_mismatches']:
            print()
            print(" ==> Chains with sequence mismatches; you will have to fix them first!\n")
            for chtype in ['protein', 'na']:
                for ch in results_dict['sequence_mismatches'].get(chtype, []):
                    print( " - %s chain %s/%s:%s  - sequence identity to reference %.2f%% [E-value=%.2e]"%
                                                                                            ("Protein" if chtype=='protein' else "Nucleic-acid",
                                                                                            ch['chain_id_reference'],
                                                                                            ch['resid_start_reference'],
                                                                                            ch['resid_end_reference'],
                                                                                            ch['seq2ref_si'],
                                                                                            ch['evalue']))
                    print(ch['alignment'])

        # 3 INDEXING_ISSUES
        if results_dict['indexing_issues']:
            print()
            print(" ==> Sequence assignment issues\n")
            for chtype in ['protein', 'na']:
                for res in results_dict['indexing_issues'].get(chtype, []):
                    print(" - %s chain fragment %s/%i-%i has a chain break at %s/%i and no residue indexing gap, check!" %
                                        ("Protein" if chtype=='protein' else "Nucleic-acid",
                                        res['chain_id_reference'],
                                        res['resid_start_reference'],
                                        res['resid_end_reference'],
                                        res['chain_id_reference'],
                                        res['break_idx']))

                    if len(res['align_sto'])<500: print(res['align_sto'])

        # 4 TRACING_ISSUES
        if results_dict['tracing_issues']:
            print()
            print(" ==> Possible tracing issues (inconsistent shifts)\n")
            for chtype in ['protein', 'na']:
                for res in results_dict['tracing_issues'].get(chtype, []):

                    print(" - %s chain fragment %s/%i-%i may be mistraced, check!" % ("Protein" if chtype=='protein' else "Nucleic-acid",
                                                                                        res['chain_id_reference'],
                                                                                        res['resid_start_reference'],
                                                                                        res['resid_end_reference']))
        # 5 REGISTER_SHIFTS
        if results_dict['register_shifts']:
            print()
            print(" ==> Sequence register shifts\n")
            for chtype in ['protein', 'na']:
                for res in results_dict['register_shifts'].get(chtype, []):

                    print(" - %s chain fragment %s/%i-%i may be shifted by %i residue%s [p-value=%.2e]" % 
                                                                                            ("protein" if chtype=='protein' else "nucleic-acid",
                                                                                            res['chain_id_reference'],
                                                                                            res['resid_start_reference'],
                                                                                            res['resid_end_reference'],
                                                                                            -res['shift'],
                                                                                            's' if abs(res['shift'])>1 else '',
                                                                                            10**-res['mlogpv']))
                    # do not show sequences for long shifts - they look ugly anyway
                    if abs(res['shift'])>10: continue


                    print( "   model seq %i-%i"%(res['resid_start_reference'], res['resid_end_reference']) )
                    print( "      ",res['model_seq'])

                    print( "     new seq %i-%i"%(res['resid_start_reference']+res['shift'], res['resid_end_reference']+res['shift']) )
                    print( "      ", res['new_seq'])
                    print()

        if results_dict['clean_report']:
            print()
            print("    No issues detected, congratulations!")
            print('\n\n')

    # -----------------------------------------------------------------------------

    def results2plot(self, results,
                           modelin              =   None,
                           rowlen               =   40,
                           logpv_threshold      =   0.55,
                           logpv_threshold_na   =   0.25):
        '''
            logpv_threshol=0.55 corresponds to a 1% one-sided confiodence interval
            i.e. in benchmark set only 1% of the fragements with wrongly assigned seuqnce had -logpv larger than this
        '''

        import matplotlib.pyplot as plt

        if self.mapin:
            title = f"checkMySequence v{cms_version} modelin: %s\nmapin: %s" % (os.path.basename(modelin), os.path.basename(self.mapin))
        else:
            title = f"checkMySequence v{cms_version} modelin: %s\nmtzin: %s" % (os.path.basename(modelin), os.path.basename(self.mtzin))

        nrows=len(results)//rowlen+1
        while nrows>100:
            rowlen+=10
            nrows=len(results)//rowlen+1

        print()
        print(" ==> Plotting results [nrows=%i]" %nrows)


        fig = plt.figure(figsize=(8,2*nrows))
        fig.subplots_adjust(bottom=0.2, left=.06, right=.99, top=.99, wspace=.4, hspace=0.7)

        axes = fig.subplots(nrows, 1)
        try:
            axes[0].set_title(title)
        except:
            axes.set_title(title)

        if nrows==1: axes=[axes]

        for irow,row in enumerate([results[_:_+rowlen] for _ in range(0, len(results), rowlen)]):

            # fill the row arraty with Nones to keep standard length
            if len(row)<rowlen: row.extend([None]*(rowlen-len(row)))

            ax = axes[irow]

            labels = ["" if _ is None else "%i:%i/%s"%(_[1], _[2], _[0]) for _ in row]
            ticks = np.array(range(len(labels)))-0.5
            color = ['gray' if _ is None or _[-1] or _[3]<logpv_threshold else 'red' for _ in row]

            ax.bar( np.array(range(rowlen))+0.5, [0 if _ is None else _[3] for _ in row], color=color)
            a=ax.get_xticks().tolist()
            ax.set_xticks(ticks)
            ax.set_xticklabels(labels)
            ax.tick_params(axis='x', labelrotation=45, length=0, labelsize=8)
            ax.tick_params(axis='y', labelsize=8)
            ax.set_xlim(-0.5, rowlen+0.5)
            ax.set_ylabel('-log(p-value)', fontsize=12)
            ax.plot([0, rowlen], [logpv_threshold, logpv_threshold], ls="--", lw=0.5, color='k')

        fig.tight_layout(h_pad=3, pad=1)
        if self.mapin:
            pdf_fname = "%s_%s.pdf" % (os.path.basename(modelin)[:-4], os.path.basename(self.mapin)[:-4])
        else:
            pdf_fname = "%s_%s.pdf" % (os.path.basename(modelin)[:-4], os.path.basename(self.mtzin)[:-4])

        plt.savefig(pdf_fname)
        print(" ==> Created output file: %s" % pdf_fname)



    # -----------------------------------------------------------------------------

    def slide4chains(self,
                     modelin             =  None,
                     selstr              =  "all",
                     seqin_fname         =  None,
                     flen_user           =  20,
                     max_flen            =  100,
                     logpv_threshold     =  0.85,
                     logpv_threshold_na  =  0.70,
                     min_chain_length    =  10,
                     plot                =  False,
                     resmapin            =  None,
                     shake_rms           =  None,
                     refseq_resi         =  False,
                     jsonout_fname       =  None,
                     debug               =  False):

        self.prepare_model(modelin, selstr=selstr)

        results = []
        chain_results = []
        frag_results = []

        _processed_ph = self.ph_selected

        if shake_rms is not None:
            print(" ==> Adding coordinate bias with %.2fA rmsd" % shake_rms)
            xyzso = xyz_shaker(_processed_ph, shake_rms)
            _processed_ph = xyzso.ph_mod

        rmo = None
        if resmapin: rmo = resmap(resmapin)

        for chain in _processed_ph.chains():

            chain_ph = iotbx.pdb.hierarchy.root()
            chain_ph.append_model(iotbx.pdb.hierarchy.model(id="0"))
            chain_ph.models()[0].append_chain(chain.detached_copy())

            # SKIP VERY SHORT PEPT/NUCS
            if chain_ph.composition().n_protein<min_chain_length and chain_ph.composition().n_nucleotide<min_chain_length:
                _rid = f"{chain.id}/{chain.residues()[0].resid().strip()}:{chain.residues()[0].resid().strip()}"
                print(f" ==> Ignoring chain fragment with less than {min_chain_length} na/aa residues: {_rid}")
                continue

            target_seqs, chain_frags, is_protein = self.find_target_sequence(seqin_fname    =   seqin_fname,
                                                                             input_ph       =   chain_ph,
                                                                             selstr         =   "all",
                                                                             hits           =   3,
                                                                             debug          =   debug)

            # find_target_sequence returns (?, [], ?) if input chain is not NA or AA (eg a ligand)
            if not chain_frags: continue
            if sum(map(len, chain_frags))<3: continue

            if not is_protein:
                flen     = max(flen_user, 30)
                min_flen = flen
            else:
                flen     = flen_user
                min_flen = 10


            if is_protein:
                selected_logpv_threshold = logpv_threshold
            else:
                selected_logpv_threshold = logpv_threshold_na

            # chain.as_sequence() puts Xs intead of ions, ligands etc that results in sequence alignment mismatches
            corrected_chain_sequence = "".join(chain.as_sequence()).replace("X","")
            if not target_seqs:
                if rmo: _locres = rmo.median_locres(chain.residues())
                print(" ==> WARNING: no reference sequence found for chain: %s [%s%s]" %
                        (chain.id, "length=%i"%len(chain.residues()), "|mean_locres=%.3f"%_locres[1] if rmo else ""))

                chain_results.append({'chain_id_reference'      : chain.id,
                                      'resid_start_reference'   : chain.residues()[0].resid().strip(),
                                      'resid_end_reference'     : chain.residues()[-1].resid().strip(),
                                      'refseq_id'               : None,
                                      'evalue'                  : None,
                                      'seq2ref_si'              : None,
                                      'chain_sequence'          : corrected_chain_sequence,
                                      'reference_sequence'      : None,
                                      'is_protein'              : is_protein})
                continue

            if rmo: _locres = rmo.median_locres(chain.residues())

            _seq2ref_si=-3
            # select target_seq with highest SI to input chain, in case there are many similar seqs in input
            for _seq in target_seqs:
                align_obj = align(seq_b=self.parse_seqstr(_seq[-1]), seq_a=corrected_chain_sequence, similarity_function="identity")
                _alignment = align_obj.extract_alignment()
                _si = 100*_alignment.calculate_sequence_identity(skip_chars=['X','N'])

                # if SIs are equal take a hit with smaller p-value
                if _si>_seq2ref_si or (_si==_seq2ref_si and _seq[1]<selected_target_seq[1]):
                    _seq2ref_si=_si
                    alignment = _alignment
                    self.prepare_target_sequence(seqin_string = _seq[-1], is_protein=is_protein)
                    selected_target_seq = _seq

            chain_results.append({'chain_id_reference'      : chain.id,
                                  'resid_start_reference'   : chain.residues()[0].resid().strip(),
                                  'resid_end_reference'     : chain.residues()[-1].resid().strip(),
                                  'refseq_id'               : selected_target_seq[0],
                                  'evalue'                  : selected_target_seq[1],
                                  'seq2ref_si'              : _seq2ref_si,
                                  'chain_sequence'          : corrected_chain_sequence,
                                  'reference_sequence'      : self.target_sequence_processed,
                                  'is_protein'              : is_protein})


            print(" ==> Reference sequence for chain %s: %s [E-value=%.2e%s%s%s]" % (chain.id,
                                                                                   selected_target_seq[0],
                                                                                   selected_target_seq[1],
                                                                                   "" if _seq2ref_si is None  else "|SI=%i"%_seq2ref_si,
                                                                                   "|length=%i"%len(chain.residues()),
                                                                                   "|mean_locres=%.3f"%_locres[1] if rmo else ""))

            if _seq2ref_si<100:
                print("\n ==> ERROR: detected sequence mismatches; ignoring chain %s\n" % chain.id)
                continue

            alignment.pretty_print(block_size=80, bottom_name="  refseq", top_name="  model")

            # skip too short chains - will not work for any chain type (aa/na) anyway
            if not is_protein and len(chain.residues())<flen: continue
            if len(chain.residues())<min_flen: continue

            # continue with fragment matching
            print()
            print(f" ==> The chain '{chain.id}' appears to be a {'protein' if is_protein else 'nucleic-acid'}. Initial test fragment length set to {flen}")
            print()

            _class = np.array([ 1.66682565, -1.13787403, -0.93516616, -0.07820552])
            # iterate over all CONTINUOUS chain fragments (there should be no breaks)
            # with at least 10 residues

            for chf in chain_frags:

                chlen = len(chf)
                if chlen<min_flen: continue

                if is_protein:
                    chf_seq = "".join([self.m2so.ogt.get(_.only_atom_group().resname, '.') for _ in chf])
                else:
                    chf_seq = "".join([self.m2so_na.ogt_na.get(_.only_atom_group().resname, '.') for _ in chf])

                print("\n ==> Processing chain fragment %i/%s - %i/%s"%(chf[0].resseq_as_int(),
                                                                        chf[0].parent().id,
                                                                        chf[-1].resseq_as_int(),
                                                                        chf[0].parent().id))

                if '.' in chf_seq:
                    print(" ==> WARNING: ignored %i non-standard residue(s) in the fargment" % chf_seq.count('.'))


                idx=0
                while idx < chlen:

                    # increase fragment length if alignment pvalue is outside 99% one-sided confidence interval
                    for flen_adj in range(flen, max_flen+1, 20):
                        real_flen = len(chf[idx:(idx+flen_adj)])
                        if is_protein:
                            logpv_array, bidx_array = self.frag2seq(chf[idx:(idx+flen_adj)], tophits=3)
                        else:
                            logpv_array, bidx_array = self.frag2seq_na(chf[idx:(idx+flen_adj)], tophits=3)


                        logpv=logpv_array[0]
                        bidx=bidx_array[0]

                        # check for sequence pseudo-repeats
                        for _bidx, _logpv in zip(bidx_array, logpv_array):
                            # check until hits are relevant
                            if _logpv<selected_logpv_threshold: break
                            m = re.match(chf_seq[idx:(idx+real_flen)], self.target_sequence_processed[_bidx:(_bidx+real_flen)])

                            if m and _logpv>selected_logpv_threshold:
                                logpv = _logpv
                                bidx = _bidx
                                break

                        if logpv>selected_logpv_threshold: break


                    # give up if requested frag exceeds the chain and is really short
                    if real_flen<min_flen: break

                    if rmo: _locres = rmo.median_locres(chf[idx:(idx+flen)])

                    seqs_match = True
                    if not re.match(chf_seq[idx:(idx+real_flen)], self.target_sequence_processed[bidx:(bidx+real_flen)]):
                        seqs_match = False

                    print("%s%8i %4i -%4i/%s -log(p-value)=%4.3f"% (" " if seqs_match else "E",
                                                                    bidx+1,
                                                                    chf[idx].resseq_as_int(),
                                                                    chf[idx+real_flen-1].resseq_as_int(),
                                                                    chf[0].parent().id, logpv),
                                                                    "%6.3f%6.3f%10i%10i"%_locres if rmo else "")

                    if not seqs_match and (logpv>selected_logpv_threshold or debug):
                        seq_match_fasta = ( chf_seq[idx:(idx+real_flen)], self.target_sequence_processed[bidx:(bidx+real_flen)] )
                        print("    m: %s\n    n: %s\n" % (chf_seq[idx:(idx+real_flen)], self.target_sequence_processed[bidx:(bidx+real_flen)]) )
                    else:
                        seq_match_fasta=None

                    if is_protein:
                        _dubious = (np.dot(_class[:3], logpv_array[:3])+_class[-1])<1 if len(logpv_array)>2 else False
                    else:
                        _dubious = False

                    frag_results.append({'match'                 : seqs_match,
                                         'mlogpv'                : logpv,
                                         'dubious'               : _dubious,
                                         'seq_assigned'          : self.target_sequence_processed[bidx:(bidx+real_flen)],
                                         'seq_reference'         : chf_seq[idx:(idx+real_flen)],
                                         'chain_id_reference'    : chf[0].parent().id,
                                         'resid_start_reference' : chf[idx].resseq_as_int(),
                                         'resid_end_reference'   : chf[idx+real_flen-1].resseq_as_int(),
                                         'length'                : real_flen,
                                         'idx_assignment'        : bidx+1,
                                         'frag_sequence'         : chf_seq[idx:(idx+real_flen)],
                                         'reference_sequence'    : self.target_sequence_processed,
                                         'is_protein'            : is_protein})



                    results.append( [chain.id, chf[idx].resseq_as_int(), chf[idx+real_flen-1].resseq_as_int(), logpv, seq_match_fasta, seqs_match] )

                    idx+=5

            # add a separator (for display only)
            results.append( None )

        # ---------------------------------------
        # done with prot/na chains - print report
        # ---------------------------------------

        if plot and results: self.results2plot(results, modelin=modelin, logpv_threshold=logpv_threshold, logpv_threshold_na=logpv_threshold_na)

        report = self.results2report(frag_results        =  frag_results,
                                     chain_results       =  chain_results,
                                     modelin             =  modelin,
                                     logpv_threshold     =  logpv_threshold,
                                     logpv_threshold_na  =  logpv_threshold_na,
                                     refseq_resi         =  refseq_resi,
                                     debug               =  debug)

        self.report2sto(report)
        if jsonout_fname:
            with open(jsonout_fname, 'w') as ofile:
                ofile.write(json.dumps(report, indent=4) if debug else json.dumps(report))
            print()
            print()
            print(f" ==> Output wrote to {jsonout_fname}")

# -----------------------------------------------------------------------------

def test():
    # tests are based on test-data included with findmysequence
    # it is a strictly requirted dep anyway

    selstr="chain A and resi 10:50"
    fmslib_root=os.path.dirname(fmslib.__file__)
    examples_dir=os.path.join(fmslib_root, 'examples')
    mapin=os.path.join(examples_dir, 'emd_3488.map')
    modelin=os.path.join(examples_dir, '5me2.pdb')


    if 0:
        # for testing only!
        so = sharpen_with_coot(mapin, badd)
        tco = so.tco
    else:
        tco = sequence_checker(mapin=mapin, badd=0)

    seqin=os.path.join(examples_dir, '5me2.fa')

    tco.prepare_model(modelin=modelin, selstr=selstr)
    res, frags, is_protein = tco.find_target_sequence(input_ph=tco.ph_selected, seqin_fname=seqin, selstr=selstr)
    tco.prepare_target_sequence(seqin_string=res[0][-1], is_protein=True)

    chf=tco.chain_frags[0]
    flen = len(chf)
    pv12, bidx=tco.frag2seq(chf)

    assert bidx == 19

    pv1, bidx=tco.frag2seq(chf[int(flen/2):])
    pv2, bidx=tco.frag2seq(chf[:int(flen/2)])

    print("   ", bidx, "pv1=%f pv2=%f pv12=%f pv123=%f"% (pv1, pv2, pv12, (pv1+pv2)/pv12))

    assert (pv1+pv2)<pv12

    print()
    print(" *** And now for something completely different")
    print()
    tco.slide4chains(modelin       =   modelin,
                     selstr        =   "chain A",
                     seqin_fname   =   seqin,
                     shake_rms     =   0.0)

    print("OK!")
    exit(0)

# -----------------------------------------------------------------------------

def pdbid(pdbid, options):
    import urllib.request,re
    import shutil

    # check if EM or MX and get EMD
    request = urllib.request.Request(f"http://www.ebi.ac.uk/pdbe/api/pdb/entry/experiment/{pdbid}")
    data = urllib.request.urlopen(request).read().decode('ascii')
    m=re.search(r'(?P<emd>EMD\-\d+)', data)
    if m:
        emdid=m.group('emd').split('-')[1]
    else:
        emdid=None


    with tempfile.TemporaryDirectory(prefix="cms_pdbid_") as tmpdirname:
        if options.keep:
            tmpdirname=options.datadir

        modelin = os.path.join(tmpdirname, pdbid+'.cif')
        if os.path.exists(modelin):
            print(f"     An existing coordinate file was found ({modelin})")
        else:
            print(f"     Downloading coordinates ({modelin})")
            with urllib.request.urlopen(f"https://files.rcsb.org/download/{pdbid}.cif") as response, \
                    open(modelin, 'wb') as out_file:
                shutil.copyfileobj(response, out_file)



        seqin = os.path.join(tmpdirname, pdbid+'.fasta')
        if os.path.exists(seqin):
            print(f"     An existing sequence file was found ({seqin})")
        else:
            print(f"     Downloading sequences to ({seqin})")
            with urllib.request.urlopen(f"http://www.ebi.ac.uk/pdbe/entry/pdb/{pdbid}/fasta") as response, \
                    open(seqin, 'wb') as out_file:
                shutil.copyfileobj(response, out_file)

        mapin,mtzin=None,None
        if emdid:
            mapin = os.path.join(tmpdirname, f"emd_{emdid}.map")
            if os.path.exists(mapin):
                print(f"     An existing map file was found ({mapin})")
            else:
                print(f"     Downloading map from EMD ({mapin})")
                with urllib.request.urlopen(f"ftp://ftp.wwpdb.org/pub/emdb/structures/EMD-{emdid}/map/emd_{emdid}.map.gz") as response, \
                        open(mapin, 'wb') as out_file:
                    shutil.copyfileobj(gzip.GzipFile(fileobj=response), out_file)

        else:
            mtzin = os.path.join(tmpdirname, pdbid+'.mtz')
            if os.path.exists(mtzin):
                print(f"     An existing mtz file was found ({mtzin})")
            else:
                print(f"     Downloading structure factors from RCSB ({mtzin})")
                with urllib.request.urlopen(f"https://edmaps.rcsb.org/coefficients/{pdbid}.mtz") as response, \
                        open(mtzin, 'wb') as out_file:
                    shutil.copyfileobj(response, out_file)

        if options.fraglen is None:
            if mapin is not None:
                options.fraglen = 20
            else:
                options.fraglen = 10

        print(f" ==> Test fragment length set to {options.fraglen}")

        time_started = time.process_time()

        tco = sequence_checker(mapin    =   mapin,
                               mtzin    =   mtzin,
                               labin    =   "FWT,PHWT",
                               badd     =   options.badd)


        tco.slide4chains(modelin       =   modelin,
                         selstr        =   options.selstr,
                         seqin_fname   =   seqin,
                         flen_user     =   options.fraglen,
                         max_flen      =   max(options.fraglen, options.max_fraglen),
                         plot          =   options.plot,
                         resmapin      =   options.resmapin,
                         shake_rms     =   options.shake_rms,
                         refseq_resi   =   options.refseq_resi,
                         jsonout_fname =   options.jsonout_fname,
                         debug         =   options.debug)

    time_finished = time.process_time()

    td=timedelta(seconds=(time_finished-time_started))
    print("\nTime elapsed %s"%str(td).split('.', 2)[0])
    print(time.strftime("Time finished %Y/%m/%d %H:%M:%S\n"))



# -----------------------------------------------------------------------------

def main():

    time_started = time.process_time()

    print(cms_header)
    if sys.platform.startswith('win'):
        HMMER_AVAILABLE = True
    elif which('hmmsearch'):
        HMMER_AVAILABLE = True
    elif CCP4_ENV and which( os.path.join(CCP4_ENV, 'libexec', 'hmmsearch') ):
        HMMER_AVAILABLE = True
    else:
        HMMER_AVAILABLE = False

    if not HMMER_AVAILABLE:
        print("ERROR: hmmer package not available")
        exit(1)

    (parser, options) = parse_args()

    if not options.debug:
        sys.tracebacklimit=0

    print( " ==> Command line: checkmysequence %s" % (" ".join(sys.argv[1:])) )


    valid_params_no = len(sys.argv[1:]) - len(parser.largs)

    # no recognized params on input, print help message and exit...
    if not valid_params_no:
        parser.print_help()
        print
        exit(0)

    if options.pdbid:
        pdbid(options.pdbid, options)
        print()
        exit(0)

    if options.test:
        test()
        print
        exit(0)


    if not (options.modelin and options.seqin and (options.mapin or options.mtzin)):
        parser.print_help()
        print
        exit(0)

    if options.fraglen is None:
        if options.mapin:
            options.fraglen = 20
        else:
            options.fraglen = 10

    print(f" ==> Test fragment length set to {options.fraglen}")

    tco = sequence_checker(mapin    =   options.mapin,
                           mtzin    =   options.mtzin,
                           labin    =   options.labin,
                           badd     =   options.badd)


    tco.slide4chains(modelin       =   options.modelin,
                     selstr        =   options.selstr,
                     seqin_fname   =   options.seqin,
                     flen_user     =   options.fraglen,
                     max_flen      =   max(options.fraglen, options.max_fraglen),
                     plot          =   options.plot,
                     resmapin      =   options.resmapin,
                     shake_rms     =   options.shake_rms,
                     refseq_resi   =   options.refseq_resi,
                     jsonout_fname =   options.jsonout_fname,
                     debug         =   options.debug)

    time_finished = time.process_time()

    td=timedelta(seconds=(time_finished-time_started))
    print("\nTime elapsed %s"%str(td).split('.', 2)[0])
    print(time.strftime("Time finished %Y/%m/%d %H:%M:%S\n"))

if __name__=="__main__":
    main()
